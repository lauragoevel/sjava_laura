import java.util.Scanner;
import java.util.Random;
//Comando compilar con acentos: javac -encoding "UTF8" *.java//

class Adivina {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        Random random = new Random();
        int incognita = random.nextInt(10)+1;
        int contador = 0;
        int num = 0;

        do {
        System.out.printf("Entra un num: ");
        
        try {
        num = keyboard.nextInt();
        
        contador = contador +1;
        
        } catch (Exception e) {
        System.out.println("***Dato incorrecto - 0 para salir***");
        keyboard.next();
        }
        
        }  while (num!=incognita);
        System.out.println("Felicidades! Tu numero es "+ num + " necesitaste "+ contador + " intentos");
        keyboard.close();
        }
    }
