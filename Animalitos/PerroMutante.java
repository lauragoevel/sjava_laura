class PerroMutante extends Perro {
    @Override
    public void ladra(){
        System.out.println("Guau! Soy un Mutante!");
    }

    @Override
    public int getNumeroPatas(){
        return super.getNumeroPatas() + 1;
    }

}