
public class Animalitos {

    public static void main(String[] args) {

    Animal Tom = new Perro(); //Instancia animal, no se puede acceder a metodos de clase Perro

        Perro Tom1 =(Perro)Tom; //Se le asigna otra instancia tipo Perro al objeto anterior.

        Tom1.ladra();
    
        System.out.println(Tom.getNumeroPatas());

    
    PerroMutante EvilTom = new PerroMutante();

        EvilTom.ladra();
    
        System.out.println(EvilTom.getNumeroPatas());


    Animal Piolin = new Pajaro();

        ((Pajaro)Piolin).vuela(); //Piolin como subclase Pajaro solo para esta llamada a método.



    }
}