import java.util.Set;
import java.util.TreeSet;


class Provincias {
    public static void main(String[] args) {
        
        Set<String> provinciasordenadas = new TreeSet<String> ();

        Datos d = new Datos(); //Objeto de la clase Datos
        String[] provincias = d.provincias(); //StringArray provincias[] del objeto Datos


        for (String s:provincias) {

            provinciasordenadas.add(s);

        }

        for (String s:provinciasordenadas)
        System.out.println(s);

   
    }
}

