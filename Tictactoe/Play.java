// clase "cargadora", simplemente ejecuta el juego
import java.util.Scanner;


class Play {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("\nIntroduzca modo de juego: \n 1 = Dos jugadores \n 2 = Maquina aleatoria \n 3 = Maquina inteligente");

        int tipodejuego = keyboard.nextInt();

        

    if(tipodejuego==1) {

        Tictactoe ttt = new Tictactoe();
        ttt.play();
    }

    else if (tipodejuego==2) {

        TictactoeRandom ttt = new TictactoeRandom();
        ttt.play();
    }

    else if (tipodejuego==3) {
        IA ttt = new IA();
        ttt.play();
    }
}}


