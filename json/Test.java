

import com.google.gson.Gson;


public class Test {
    public static void main(String[] args) {
        Gson gson = new Gson();

        Persona p = new Persona("humbert", 22); 
        gson.toJson(p, System.out); //coge el objeto, lo convierte a String y le aplica el Sysout
        
        String str = gson.toJson(p); //convierte en un string que va a tener formarto Json el objeto p
        System.out.println(str);

        // {"nombre":"humbert","edad":22}
        String js = "{\"nombre\":\"humbert\",\"edad\":22}";
        Persona p2 = gson.fromJson(js, Persona.class);
        System.out.println(p2.nombre);

         // {"nombre":"humbert","edad":22}
         String js2String = "{\"nombre\":\"antonio\",\"edad\":22}";
         Persona p3 = gson.fromJson(js, Persona.class);
         System.out.println(p3.toString());
    }
}