package com.sjava.web;


import java.util.ArrayList;

public class PersonaController {
    private static ArrayList<Persona> contactos = new ArrayList<Persona>();
    private static int contador = 0;

    public static void muestraContactos() {
        for (Persona p : contactos) {
            System.out.println(p);
        }
    }

    public static ArrayList<Persona> getContactos() {
        return contactos;
    }

    public static void nuevoContacto(Persona pers) {
        contador++;
        pers.setId(contador);
        contactos.add(pers);
    }

    public static int numContactos() {
        return contactos.size();
    }

    public static void muestraContactoId(int id) {
        for (Persona p : contactos) {

            if (id == p.getId()) {
                System.out.println(p);
                return;
            }
        }
    }

    public static void borraContactoId(int id) { //a veces no funciona al iterar, hay que crear una persona null fuera del bucle, asignarla en el bucle y borrarla despues
        for (Persona p : contactos) {

            if (id == p.getId()) {
                contactos.remove(p);
                return;
            }
        }
    }

}
