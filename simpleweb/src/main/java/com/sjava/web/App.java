package com.sjava.web;

public class App 
{
    public static void main( String[] args )
    {
        new Persona("ana", 1995, "ana@gmail.com");
        new Persona("carmen", 1999, "carmen@gmail.com");
        new Persona("adela", 1997, "adela@gmail.com");
        new Persona("azucena", 1991, "azucena@gmail.com");
        new Persona("aurora", 1990, "aurora@gmail.com");

        System.out.println("....................................");
        PersonaController.muestraContactos();
        System.out.println("....................................");
        PersonaController.muestraContactoId(4);
        System.out.println("....................................");
        PersonaController.borraContactoId(3);
        PersonaController.muestraContactos();
        System.out.println("....................................");
    
    }
}
