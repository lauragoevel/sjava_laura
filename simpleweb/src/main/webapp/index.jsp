
<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.Persona.java" %>


<%
  String titulo="Hola, hago un jsp" ;
  String[] lista= new String[] {"Primero", "Segundo", "Tercero"};
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Ejercicios HMTL/CSS</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>

<h1><% out.print(titulo); %></h1> <%-- <%= es equivalente a <% out.print() --%>
<h1><%=titulo%></h1>


<ul>
<% for (String s: lista){ %>
    <li> <%=s%> </li>

<% } %>
</ul>

<br>


</body>
</html>
