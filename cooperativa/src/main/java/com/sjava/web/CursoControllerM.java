package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class CursoControllerM {

    private static List<Curso> listaCursos = new ArrayList<Curso>();
    private static int contador = 0;

    static {

        // Curso a = new Curso(99,"ricard hernández", "algo@algo.com", "999333");
        // listaCursos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Curso> getAll(){
        return listaCursos;
    }

    //getId devuelve un registro
    public static Curso getId(int id){
        for (Curso cu : listaCursos) {
            if (cu.getId()==id){
                return cu;
            }
        }
        return null;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Curso curso) {
        if (curso.getId() == 0){
            contador++;
            curso.setId(contador);
            listaCursos.add(curso);
        } else {
            for (Curso cursoi : listaCursos) {
                if (cursoi.getId()==curso.getId()) {
                   
                    cursoi.setNombre(curso.getNombre());
                    cursoi.setHoras(curso.getHoras());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de alumnos
    public static int size() {
        return listaCursos.size();
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        Curso borrar=null;
        for (Curso c : listaCursos) {
            if (c.getId()==id){
                borrar = c;
                break;
            }
        }
        if (borrar!=null) {
            listaCursos.remove(borrar);
        }
    }

}