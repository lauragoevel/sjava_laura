package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class ProfesorControllerM {

    private static List<Profesor> listaProfesores = new ArrayList<Profesor>();
    private static int contador = 0;

    static {

        // Profesor a = new Profesor(99,"ricard hernández", "algo@algo.com", "999333");
        // listaProfesores.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Profesor> getAll(){
        return listaProfesores;
    }

    //getId devuelve un registro
    public static Profesor getId(int id){
        for (Profesor profe : listaProfesores) {
            if (profe.getId()==id){
                return profe;
            }
        }
        return null;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Profesor profe) {
        if (profe.getId() == 0){
            contador++;
            profe.setId(contador);
            listaProfesores.add(profe);
        } else {
            for (Profesor profei : listaProfesores) {
                if (profei.getId()==profe.getId()) {
                   
                    profei.setNombre(profe.getNombre());
                    profei.setEmail(profe.getEmail());
                    profei.setTelefono(profe.getTelefono());
                    profei.setEspecialidad(profe.getEspecialidad());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de alumnos
    public static int size() {
        return listaProfesores.size();
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        Profesor borrar=null;
        for (Profesor p : listaProfesores) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaProfesores.remove(borrar);
        }
    }

}