public class Mujer extends Person {

    public Mujer (String nombre, int edad){
        super(nombre, edad);
    }
    @Override
    public String getNombre(){
        return "soy la Sra." +nombre;
    } 

    @Override
    public int getEdad(){
    
        if (super.edad > 45){
        return edad-5;
    }
        return edad;
    } 

}