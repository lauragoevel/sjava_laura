public class Person {

    protected String nombre;
    protected int edad;

public Person (String nombre, int edad) {
    this.nombre=nombre;
    this.edad=edad;
}

public String getNombre(){
    return nombre;
} 

public int getEdad(){
    return edad;
} 

public void setNombre(String nombre) {
    this.nombre=nombre;
}

public void setEdad(int edad) {
    this.edad=edad;
}

public String presentacion(){
    return "Hola! "+getNombre()+" y tengo "+getEdad()+ " años";
}

}