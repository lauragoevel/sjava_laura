import java.util.Date;

class Test {

    public static void main(String[] args) {

        // DateUtil du = new DateUtil();

        Date d = DateUtil.createDate(2018, 6, 9);
        String s = DateUtil.dateformat(d, "dd-yyy-MMMM");
        System.out.println(s);

        StringBuilder sb = new StringBuilder();

        sb.append ("La fecha es ");
        sb.append (DateUtil.dateformat(d, "dd"));
        sb.append (" de ");
        sb.append (DateUtil.dateformat(d, "MMMM"));
        sb.append (" del ");
        sb.append (DateUtil.dateformat(d, "yyy"));

        System.out.println(sb);

        int domingosjunio = DateUtil.cuentadomingos(2018,6);
        System.out.println("Los domingos de junio de 2018 son "+domingosjunio);

       


    }

}