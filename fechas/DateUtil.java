import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class DateUtil {

    private DateUtil(){}

    public static Date createDate(int anyo, int mes, int dia) {

        Calendar cal = Calendar.getInstance();
        cal.set(anyo,mes-1,dia);
        return cal.getTime();

    }

public static String dateformat(Date date, String format) {

SimpleDateFormat sdf = new SimpleDateFormat(format);
String date1 = sdf.format(date);

return date1;

}

public static int cuentadomingos (int ano, int mes){
    int domingos = 0;
    Calendar cal = Calendar.getInstance();
    cal.set(ano, mes-1, 1);

    while (cal.get (Calendar.MONTH)==mes-1){
        if (cal.get(Calendar.DAY_OF_WEEK)==1){
            domingos++;
        }
        cal.add(Calendar.DATE,1);
    }

return domingos;

}


}