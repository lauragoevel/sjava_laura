package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class CursoController {
    
    private static List<Curso> listaCursos = new ArrayList<Curso>();
    private static int contador = 0;

    static {

        // Alumno a = new Alumno(99,"ricard hernández", "algo@algo.com", "999333");
        // listaAlumnos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Curso> getAll(){
        return listaCursos;
    }

    //getId devuelve un registro
    public static Curso getId(int id){
        for (Curso p : listaCursos) {
            if (p.getId()==id){
                return p;
            }
        }
        return null;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Curso cu) {
        if (cu.getId() == 0){
            contador++;
            cu.setId(contador);
            listaCursos.add(cu);
        } else {
            for (Curso cur : listaCursos) {
                if (cur.getId()==cu.getId()) {
                   
                    cur.setNombre(cu.getNombre());
                    cur.setHoras(cu.getHoras());
                    cur.setIdprofesor(cu.getIdprofesor());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de alumnos
    public static int size() {
        return listaCursos.size();
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        Curso borrar=null;
        for (Curso p : listaCursos) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaCursos.remove(borrar);
        }
    }

}