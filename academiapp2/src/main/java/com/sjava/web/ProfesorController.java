package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class ProfesorController {
    
    private static List<Profesor> listaProfesor = new ArrayList<Profesor>();
    private static int contador = 0;

    static {

        // Alumno a = new Alumno(99,"ricard hernández", "algo@algo.com", "999333");
        // listaAlumnos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Profesor> getAll(){
        return listaProfesor;
    }

    //getId devuelve un registro
    public static Profesor getId(int id){
        for (Profesor p : listaProfesor) {
            if (p.getId()==id){
                return p;
            }
        }
        return null;
    }

    public static String getNombreid(int id){
        for (Profesor p : listaProfesor) {
            if (p.getId()==id){
                return p.getNombre();
            }
        }
        return null;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Profesor pro) {
        if (pro.getId() == 0){
            contador++;
            pro.setId(contador);
            listaProfesor.add(pro);
        } else {
            for (Profesor prof : listaProfesor) {
                if (prof.getId()==pro.getId()) {
                   
                    prof.setNombre(pro.getNombre());
                    prof.setEmail(pro.getEmail());
                    prof.setTelefono(pro.getTelefono());
                    prof.setEspecialidad(pro.getEspecialidad());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de alumnos
    public static int size() {
        return listaProfesor.size();
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        Profesor borrar=null;
        for (Profesor p : listaProfesor) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaProfesor.remove(borrar);
        }
    }

}