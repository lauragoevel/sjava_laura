package com.sjava.web;

public class Curso {

    private int id;
    private String nombre;
    private String horas;
    private int idprofesor;

    public Curso (String nombre, String horas, int idprofesor) {
        this.nombre = nombre;
        this.horas = horas;
        this.idprofesor = idprofesor;

        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de AlumnoController
        // AlumnoController.nuevoContacto(this);
    }

    public Curso (int id, String nombre, String horas, int idprofesor) {
        this.id = id;
        this.nombre = nombre;
        this.horas = horas;
        this.idprofesor = idprofesor;
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getHoras(){
        return this.horas;
    }

    protected void setHoras(String horas){
        this.horas = horas;
    }


    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }

    public int getIdprofesor(){
        return this.idprofesor;
    }

    protected void setIdprofesor(int idprofesor){
        this.idprofesor=idprofesor;
    }


  
}